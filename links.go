package main

import (
	"encoding/json"
	"io/ioutil"
	"net/url"
	"os"
	"sync"
	"time"
)

type State struct {
	LastSeen time.Time `json:"lastSeen"`
	Alive    bool      `json:"alive"`
	count    int
}

type Links struct {
	L map[string]State `json:"links"`
	m sync.RWMutex
}

func LoadLinks(f *os.File) (*Links, error) {
	jsonData, err := ioutil.ReadAll(f)
	if err != nil {
		return nil, err
	}
	links := &Links{}
	err = json.Unmarshal(jsonData, links)
	if err != nil {
		return nil, err
	}
	return links, nil
}

func (l *Links) NewLink(link string) string {
	u, err := url.Parse(link)
	if err != nil {
		elog.Println(err)
		link = "http://" + link
	} else if u.Scheme != "http" && u.Scheme != "https" {
		link = "http://" + link
	}
	for k := range l.L {
		if k == link {
			return ""
		}
	}
	l.m.Lock()
	l.L[link] = State{time.Now(), true, 0}
	l.m.Unlock()
	return link
}

func (l *Links) RemoveLink(link string) string {
	l.m.Lock()
	defer l.m.Unlock()
	u, err := url.Parse(link)
	if err != nil {
		elog.Println(err)
		link = "http://" + link
	} else if u.Scheme != "http" && u.Scheme != "https" {
		link = "http://" + link
	}
	for k := range l.L {
		if k == link {
			delete(l.L, link)
			return link
		}
	}
	return ""
}

func (l *Links) GetLinks() []string {
	links := make([]string, 0, len(l.L))
	l.m.RLock()
	for k := range l.L {
		links = append(links, k)
	}
	l.m.RUnlock()
	return links
}

func (l *Links) GetState(link string) State {
	l.m.RLock()
	state := l.L[link]
	l.m.RUnlock()
	return state
}

func (l *Links) SetState(link string, state State) {
	l.m.Lock()
	defer l.m.Unlock()
	l.L[link] = state
}

func (l *Links) Save(fn string) error {
	f, err := os.Create(fn)
	if err != nil {
		return err
	}
	defer f.Close()
	encoder := json.NewEncoder(f)
	l.m.RLock()
	encoder.Encode(l)
	l.m.RUnlock()
	return nil
}

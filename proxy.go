package main

import (
	"context"
	"fmt"
	"net"
	"net/http"
	"net/url"
	"os"

	"golang.org/x/net/proxy"
)

func ProxyAwareClient() *http.Client {
	var d proxy.Dialer
	d = proxy.Direct
	proxyServer, isSet := os.LookupEnv("HTTP_PROXY")
	if isSet {
		proxyURL, err := url.Parse(proxyServer)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Invalid proxy url %q\n", proxyURL)
		}
		d, err = proxy.FromURL(proxyURL, proxy.Direct)
	}
	httpTransport := &http.Transport{}
	httpClient := &http.Client{Transport: httpTransport}
	httpTransport.DialContext =
		func(ctx context.Context, network, addr string) (net.Conn, error) {
			conn, err := d.Dial(network, addr)
			return conn, err
		}
	return httpClient
}

package main

import (
	"crypto/tls"
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"sync"
	"time"

	"github.com/go-telegram-bot-api/telegram-bot-api"
	"golang.org/x/net/proxy"
)

var elog *log.Logger
var ilog *log.Logger
var dlog *log.Logger
var logMutex sync.Mutex
var tg *Telegram
var ll *Links
var c *Config
var lFile string
var logFile *os.File

func init() {
	var err error
	elog = log.New(os.Stdout, "ERROR: ", log.Lshortfile)
	ilog = log.New(os.Stdout, "INFO: ", log.Lshortfile)
	dlog = log.New(os.Stdout, "DEBUG ", log.Lshortfile)

	var cFile string
	flag.StringVar(&cFile, "c", "config.json", "config file")
	flag.StringVar(&lFile, "l", "links.json", "links file")
	flag.Parse()

	c, err = getConfig(cFile)
	if err != nil {
		elog.Fatal(err)
	}
	ll, err = getLinks(lFile)
	if err != nil {
		elog.Fatal(err)
	}
}

func main() {
	var err error
	logFile, err = os.Create("warn.log")
	if err != nil {
		log.Fatal(err)
	}
	defer logFile.Close()
	elog = log.New(logFile, "ERROR: ", log.Lshortfile)
	bot, err := tgNew(c.Token, elog)
	if err != nil {
		ilog.Println(err)
		elog.Fatal(err)
		return
	}
	ilog.Println(bot)
	bot.Debug = false

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	freq := time.NewTicker(time.Duration(c.Freq) * time.Second)
	//onceADay := time.NewTicker(1 * time.Second)
	onceADay := time.NewTicker(24 * time.Hour)

	msgCh := make(chan string)

	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs)
	go func() {
		sig := <-sigs
		logMutex.Lock()
		elog.Println(sig)
		elog.Println("Program was terminated at:", time.Now().Format("15:04:05"))
		logMutex.Unlock()
		freq.Stop()
		onceADay.Stop()
		close(msgCh)
		ilog.Println("Saving link list")
		ll.Save(lFile)
		os.Exit(-1)
	}()

	go func() {
		for range freq.C {
			check(msgCh, false)
		}
	}()

	go func() {
		for range onceADay.C {
			check(msgCh, true)
		}
	}()

	go bot.Sender(c.AdminIds, msgCh)

	updates, err := bot.GetUpdatesChan(u)
	if err != nil {
		elog.Fatal(err)
		return
	}
	// MAIN LOOP
	for update := range updates {
		in := update.Message
		if in == nil {
			continue
		}
		uid := in.Chat.ID
		if in.IsCommand() && isAdmin(uid) {
			switch in.Command() {
			case "start":
				bot.SendText(uid, "Welcome...")
			case "add":
				//add new link
				link := in.CommandArguments()
				link = strings.Replace(link, " ", "", -1)
				link = ll.NewLink(link)
				if link == "" {
					bot.SendText(uid, "Nothing to add.")
				} else {
					ll.Save(lFile)
					bot.SendText(uid, link+" was added.")
				}
			case "remove":
				//remove link
				link := in.CommandArguments()
				link = strings.Replace(link, " ", "", -1)
				link = ll.RemoveLink(link)
				if link == "" {
					bot.SendText(uid, "Nothing to remove.")
				} else {
					bot.SendText(uid, link+" was removed.")
				}
			case "list":
				links := ll.GetLinks()
				text := ""
				for _, link := range links {
					text = fmt.Sprintf("%s\n%s", text, link)
				}
				bot.SendText(uid, text)
			}
		}
	}
}

func isAdmin(uid int64) bool {
	for _, id := range c.AdminIds {
		if uid == id {
			return true
		}
	}
	return false
}

func req(link string) int {
	var d proxy.Dialer
	d = proxy.Direct
	httpTransport := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	httpClient := &http.Client{Transport: httpTransport}
	httpTransport.Dial = d.Dial
	r, err := httpClient.Get(link)
	if err != nil {
		logMutex.Lock()
		elog.Println(err)
		logMutex.Unlock()
		return 0
	}
	if r.StatusCode != 200 {
		defer r.Body.Close()
		logMutex.Lock()
		dlog.Println(r.StatusCode)
		logMutex.Unlock()
		io.Copy(os.Stdout, r.Body)

	}
	return r.StatusCode
}

func check(mch chan<- string, talkative bool) {
	links := ll.GetLinks()
	defer func() {
		if recover() != nil {
			ilog.Println("Saving link list")
			ll.Save(lFile)
		}
	}()
	for _, l := range links {
		s := req(l)
		state := ll.GetState(l)
		if s == 200 && !state.Alive { //site is alive againg
			state.Alive = true
			state.LastSeen = time.Now()
			state.count = 0
			ll.SetState(l, state)
			mch <- fmt.Sprintf("%s %s", l, " ♥️ SITE IS ALIVE NOW !!!")
			continue
		}
		if s == 200 && state.Alive { //site is still alive
			state.LastSeen = time.Now()
			ll.SetState(l, state)
			if talkative {
				mch <- fmt.Sprintf("%s %s", l, " site is alive")
			}
			continue
		}
		if s != 200 && state.Alive { //site is down
			state.count = state.count + 1
			ll.SetState(l, state)
			if state.count > c.MaxTries {
				state.Alive = false
				ll.SetState(l, state)
				mch <- fmt.Sprintf("%s %s %s", "⚠️ WARNING: ", l, " SITE IS DOWN NOW !!!")
			}
			continue
		}
		if s != 200 && !state.Alive && time.Since(state.LastSeen) > time.Duration(c.MaxTime)*time.Second { //site is down more for than MaxTime seconds
			mch <- fmt.Sprintf("site %s is down for more than %d seconds", l, c.MaxTime)
			continue
		}
	}
}

func getConfig(fn string) (*Config, error) {
	fh, err := os.Open(fn)
	if err != nil {
		return nil, err
	}
	defer fh.Close()
	cfg, err := LoadConfig(fh)
	if err != nil {
		return nil, err
	}
	return cfg, nil
}

func getLinks(fn string) (*Links, error) {
	fh, err := os.Open(fn)
	if err != nil {
		return nil, err
	}
	defer fh.Close()
	links, err := LoadLinks(fh)
	if err != nil {
		return nil, err
	}
	return links, nil
}

package main

import (
	"log"
	"net/http"

	"github.com/go-telegram-bot-api/telegram-bot-api"
)

type Telegram struct {
	*tgbotapi.BotAPI
	client *http.Client
}

func tgNew(token string, logger *log.Logger) (*Telegram, error) {
	client := ProxyAwareClient()
	bot, err := tgbotapi.NewBotAPIWithClient(token, client)
	if err != nil {
		return nil, err
	}
	err = tgbotapi.SetLogger(logger)
	if err != nil {
		log.Println(err)
		return nil, err
	}
	return &Telegram{bot, client}, nil
}

func (t *Telegram) SendText(id int64, text string) {
	msg := tgbotapi.NewMessage(id, text)
	msg.DisableWebPagePreview = true
	t.Send(msg)
}

func (t *Telegram) Sender(aids []int64, msgCh <-chan string) {
	for msg := range msgCh {
		for _, id := range aids {
			t.SendText(id, msg)
		}
	}
}

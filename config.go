package main

import (
	"encoding/json"
	"io/ioutil"
	"os"
)

type Config struct {
	Token    string  `json:"token"`
	AdminIds []int64 `json:"admin_ids"`
	MaxTime  int     `json:"maxtime"`
	MaxTries int     `json:"maxtries"`
	Freq     int     `json:"frequency"`
}

func LoadConfig(f *os.File) (*Config, error) {
	jsonData, err := ioutil.ReadAll(f)
	if err != nil {
		return nil, err
	}
	config := &Config{}
	err = json.Unmarshal(jsonData, config)
	if err != nil {
		return nil, err
	}
	return config, nil
}
